import React, { useState, useContext } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { AuthProvider } from "./context/AuthContext";

import Welcome from "./components/Welcome";
import Practice from "./components/Practice";
import NotFound from "./components/NotFound";
import FrequentlyAskedQuestions from "./components/FrequentlyAskedQuestions";
import Home2 from "./components/Home2";
import Login from "./components/Login";
import ContactRHF from "./components/ContactRHF";
import SignUp2 from "./components/SignUp2";
import About2 from "./components/About2";
import PrivateRoute from "./firebaseComponents/PrivateRoute";
import Account from "./components/Account";
import AuthHome from "./components/AuthRoutes/AuthHome";
import Clients from "./components/AuthRoutes/Clients";
import Info from "./components/AuthRoutes/Info";
import VideoBackground from "./components/VideoBackground";
import NavPractice from "./components/dataComponents/NavPractice";
import FancyNav from "./components/dataComponents/FancyNav";
import NavSlide from "./components/dataComponents/NavSlide";
import ExamplePractice from "./components/ExamplePractice";
import Blog from "./components/Blog";
import ArticleComp from "./components/ArticleComp";
import ExampleQP from "./components/ExampleQP";
import Article from "./components/basicComps/Article";
import ArticleCreate from "./components/AuthRoutes/ArticleCreate";
import ExampleTodo from './components/ExampleToDo'

import { ColorContext } from "./ColorContext";
import Forms from "./components/Forms";

const App = () => {
  return (
    <div>
      <Router>
        <AuthProvider>
          <Switch>
            <PrivateRoute exact path="/account" component={Account} />
            <PrivateRoute path="/authhome" component={AuthHome} />
            <PrivateRoute path="/clients" component={Clients} />
            <PrivateRoute path="/info" component={Info} />
            <Route path="/" exact component={Welcome}></Route>
            <Route path="/home" exact component={Home2}></Route>
            <Route path="/about" exact component={About2}></Route>
            <Route path="/practice" exact component={Practice}></Route>
            <Route path="/navpractice" exact component={NavPractice}></Route>
            <Route
              path="/examplebackground"
              exact
              component={VideoBackground}
            ></Route>
            <Route path="/fancynav" exact component={FancyNav}></Route>
            <Route path="/navslide" exact component={NavSlide}></Route>
            <Route
              path="/faq"
              exact
              component={FrequentlyAskedQuestions}
            ></Route>
            <Route path="/login" exact component={Login}></Route>
            <Route path="/signup" component={SignUp2}></Route>
            <Route path="/contact" exact component={ContactRHF}></Route>
            <Route
              path="/examplepractice"
              exact
              component={ExamplePractice}
            ></Route>
            <Route path="/blog" exact component={Blog}></Route>
            <Route path="/forms" exact component={Forms}></Route>
            <Route path="/exampleparams" exact component={ExampleQP}></Route>
            <Route path="/exampletodo" exact component={ExampleTodo}></Route>
            <Route path="/ac" exact component={ArticleCreate}></Route>
            <Route path="/article" exact component={ArticleComp}></Route>
            <Route path="example/:id" exact children={<ArticleComp />}></Route>
            <Route path="/article/:id" exact children={<Article />}></Route>

            <Route path="*" component={NotFound}></Route>
          </Switch>
        </AuthProvider>
      </Router>
    </div>
  );
};

export default App;
