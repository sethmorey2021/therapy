import React from "react";
import ReactDOM from "react-dom";
// import { BrowserRouter } from "react-router-dom";
import {ColorContext} from './ColorContext'

import App from "./App";
import "./App.css";

ReactDOM.render(
  // <BrowserRouter>
    <App />,
  // </BrowserRouter>,
  document.querySelector("#root")
);

// ReactDOM.render(<App />, document.querySelector("#root"));
