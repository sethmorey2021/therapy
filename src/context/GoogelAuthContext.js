import React, { useContext, useState, useEffect } from "react";
import { auth } from "../firebase";

// auth().signInWithPopup(provider).then(function(result) {
//   var token =
// })

// Using a popup.

const AuthContext = React.createContext();

export function useAuth() {
  return useContext(AuthContext);
}

export function AuthProvider({ children }) {
  const [currentUser, setCurrentUser] = useState();
  const [loading, setLoading] = useState(true);

  var googleAuthProvider = auth.GoogleAuthProvider();

  googleAuthProvider.addScope("profile");
  googleAuthProvider.addScope("email");

  const googleAuth = auth()
    .signInWithPopup(googleAuthProvider)
    .then(function (result) {
      // This gives you a Google Access Token.
      var gToken = result.credential.accessToken;
      // The signed-in user info.
      var gUser = result.user;
    });

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((user) => {
      setCurrentUser(googleAuth);
      setLoading(false);
    });

    return unsubscribe;
  }, [googleAuth]);

  const value = {
    currentUser,
  };

  return <AuthContext.Provider value={value}>{!loading && children}</AuthContext.Provider>;
}
