import React, { useEffect, useState } from "react";
import { db } from "../Firebase";

const booksRef = db.collection("blogs");

booksRef.get().then((snapshot) => {
  const data = snapshot.docs.map((doc) => ({
    id: doc.id,
    ...doc.data(),
  }));
  console.log("All data in 'books' collection", data);
  // [ { id: 'glMeZvPpTN1Ah31sKcnj', title: 'The Great Gatsby' } ]
});

export const defaultBlogContext = React.createContext();
