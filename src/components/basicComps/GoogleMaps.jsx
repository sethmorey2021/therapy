import React from 'react';
import { GoogleMap, LoadScript } from '@react-google-maps/api';

const MapContainer = () => {
  
  const mapStyles = {        
    height: "100vh",
    width: "100%"};
  
  const defaultCenter = {
    lat: 33.64380657433918, lng:-112.23053253107545
  }
  
  return (
     <LoadScript
       googleMapsApiKey='AIzaSyCkYmfZyrGsbpXVXdaireh585fr2V5-NfA'>
        <GoogleMap
          mapContainerStyle={mapStyles}
          zoom={18}
          center={defaultCenter}
        />
     </LoadScript>
  )
}

export default MapContainer;