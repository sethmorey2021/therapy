import React from "react";
import { useParams } from 'react-router';

const Article = ({ title, author, content, blogId }) => {

  return (
    <>
      <img src="https://cdn.pixabay.com/photo/2017/08/13/16/43/notebook-2637757_960_720.jpg" />
      <h1>{title}</h1>
      <h2>{author}</h2>
      <p>{content}</p>
    </>
  );
};

export default Article;
