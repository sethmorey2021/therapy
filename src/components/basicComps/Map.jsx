import React from "react";
import GoogleMapReact from "google-map-react";

const apiKey = process.env.GOOGLE_API_KEY;

const Map = ({ center, zoom }) => {
  return (
    <div style={{ width: "100%", height: "100vh" }}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: "AIzaSyCkYmfZyrGsbpXVXdaireh585fr2V5-NfA" }}
        defaultCenter={center}
        defaultZoom={zoom}
      ></GoogleMapReact>
    </div>
  );
};

export default Map;
