import React from "react";
import { Link } from "react-router-dom";

const Card = ({ title, author, content, blogId }) => {

  // let uppperCaseAuthor = author.toUpperCase();

  return (
    <>
      <div class="card">
        <div class="card-image">
          <figure class="image is-4by3">
            <img
              src="https://bulma.io/images/placeholders/1280x960.png"
              alt="Placeholder image"
            />
          </figure>
        </div>
        <div class="card-content">
          <Link to={`/article/${blogId}`}>
            <p class="title">{title}</p>
          </Link>
          <p>By: {author}</p>
          <p>{content}</p>
        </div>
        <footer class="card-footer">
          <p class="card-footer-item">
            <span>
              Author, Date
              <a href="https://twitter.com/codinghorror/status/506010907021828096">
                {" "}
                Twitter
              </a>
            </span>
          </p>
        </footer>
      </div>
    </>
  );
};

export default Card;
