import React from "react";
import Card from "../components/basicComps/Card";
import BasicHeader from "../components/BasicHeader";
import Map from "./basicComps/Map";
import GoogleMaps from './basicComps/GoogleMaps'

const ipsumLorem =
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pulvinar nunc ac posuere iaculis. Suspendisse ac justo malesuada quam vestibulum consequat.";

const FrequentlyAskedQuestions = () => {
  const center = { center: { lat: 42.3265, lng: -122.8756 } };
  const zoom = { zoom: 5 };

  return (
    <>
      <BasicHeader />
      <section
        class="hero is-primary is-medium"
        className="medium-hero"
        style={{ marginBottom: "25px" }}
      ></section>

      <div className="gutter">
        <div style={{ marginBottom: "10px" }}>
          Danielle sees clients on Tuesdays.
        </div>
        <div style={{ marginBottom: "10px" }}>
          Her practicum students and Licensed Associate Counselors see clients
          on Mondays, Tuesdays, Wednesdays, Thursdays, Fridays, and Saturdays at
          this location, our Surprise location, our Glendale location and our
          Goodyear location.
        </div>
        <p style={{ marginBottom: "10px" }}>
          The Atrium at Arrowhead 17505 N. 79th Avenue, Suite 213C Glendale,
          Arizona 85308
        </p><GoogleMaps/>
      </div>

      

      <p>accordian</p>
    </>
  );
};

export default FrequentlyAskedQuestions;
