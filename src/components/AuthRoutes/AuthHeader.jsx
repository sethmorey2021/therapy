import React from "react";
import { Link } from 'react-router-dom';

const BasicHeader = () => {
  return (
    <div>
      <nav
        class="navbar"
        role="navigation"
        aria-label="main navigation"
        style={{ backgroundColor: "black" }}
      >
        <div class="navbar-brand">
          <div class="navbar-item" href="https://bulma.io">
            <div className="custom-header" style={{ color: "white" }}>
              Danielle Cutrer
            </div>
          </div>

          <div
            role="button"
            class="navbar-burger burger"
            aria-label="menu"
            aria-expanded="false"
            data-target="navbarBasicExample"
          >
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </div>
        </div>

        <div id="navbarBasicExample" class="navbar-menu">
          <div class="navbar-start">
            <div class="navbar-item has-dropdown is-hoverable">
              <div class="navbar-link" style={{ color: "white", fontWeight: "500" }}>
                Options
              </div>

              <div class="navbar-dropdown">
                <Link to="/authhome">
                  <a class="navbar-item">Dash</a>
                </Link>
                <Link to="/clients">
                  <a class="navbar-item">Clients</a>
                </Link>
                <Link to="/info">
                  <a class="navbar-item">Info</a>
                </Link>
                <hr class="navbar-divider" />
                <Link to="/account">
                  <a class="navbar-item">Account</a>
                </Link>
              </div>
            </div>
          </div>

          <div class="navbar-end">
            <div class="navbar-item">
              <div class="buttons">
                <Link to="/login">
                  <button class="button is-primary">
                    <strong>Log Out</strong>
                  </button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default BasicHeader;
