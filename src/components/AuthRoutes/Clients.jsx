import React from 'react'
import AuthHeader from './AuthHeader'

export default function Clients() {
  return (
    <div>
      <AuthHeader/>
      <div className="gutter">clients</div>
    </div>
  )
}