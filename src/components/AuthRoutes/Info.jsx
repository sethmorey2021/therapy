import React from 'react'
import AuthHeader from './AuthHeader'

export default function Info() {
  return (
    <div>
      <AuthHeader/>
      <div className="gutter">Info</div>
    </div>
  )
}
