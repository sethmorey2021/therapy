import React from 'react'
import AuthHeader from '../AuthRoutes/AuthHeader'

const AuthHome = () => {
  return (
    <>
      <AuthHeader />
      <div>
        <div class="container is-fluid">
          <div class="tile is-ancestor">
            <div className="tile is-parent">
              <article className="tile is-child box">
                <p className="title">Hello World</p>
                <p className="subtitle">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus consectetur hic
                  nostrum laudantium qui eligendi fugiat. Nostrum, laboriosam voluptates mollitia
                  sapiente vero quis explicabo quod. Quas numquam distinctio quos reiciendis?
                </p>
                <p className="subtitle">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus consectetur hic
                  nostrum laudantium qui eligendi fugiat. Nostrum, laboriosam voluptates mollitia
                  sapiente vero quis explicabo quod. Quas numquam distinctio quos reiciendis?
                </p>
                <p className="subtitle">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus consectetur hic
                  nostrum laudantium qui eligendi fugiat. Nostrum, laboriosam voluptates mollitia
                  sapiente vero quis explicabo quod. Quas numquam distinctio quos reiciendis?
                </p>
              </article>
            </div>
            <div className="tile is-parent">
              <article className="tile is-child box">
                <p className="title">Hello World</p>
                <p className="subtitle">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus consectetur hic
                  nostrum laudantium qui eligendi fugiat. Nostrum, laboriosam voluptates mollitia
                  sapiente vero quis explicabo quod. Quas numquam distinctio quos reiciendis?
                </p>
              </article>
            </div>
          </div>

          <div class="tile is-ancestor">
            <div className="tile is-parent">
              <article className="tile is-child box">
                <p className="title">Hello World</p>
                <p className="subtitle">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus consectetur hic
                  nostrum laudantium qui eligendi fugiat. Nostrum, laboriosam voluptates mollitia
                  sapiente vero quis explicabo quod. Quas numquam distinctio quos reiciendis?
                </p>
              </article>
            </div>
            <div className="tile is-parent">
              <article className="tile is-child box">
                <p className="title">Hello World</p>
                <p className="subtitle">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus consectetur hic
                  nostrum laudantium qui eligendi fugiat. Nostrum, laboriosam voluptates mollitia
                  sapiente vero quis explicabo quod. Quas numquam distinctio quos reiciendis?
                </p>
              </article>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default AuthHome
