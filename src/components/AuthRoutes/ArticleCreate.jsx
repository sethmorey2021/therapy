import React, { useState } from "react";
import { useForm } from "react-hook-form";
import BasicHeader from "../BasicHeader";
import { db } from "../../Firebase";
import List from "../basicComps/List";

const data = [
  {
    id: 1,
    dataItem: "Give dog a bath",
    complete: true,
  },
  {
    id: 2,
    dataItem: "Do laundry",
    complete: true,
  },
  {
    id: 3,
    dataItem: "Vacuum floor",
    complete: false,
  },
  {
    id: 4,
    dataItem: "Feed cat",
    complete: true,
  },
  {
    id: 5,
    dataItem: "Change light bulbs",
    complete: false,
  },
];

const ArticleCreate = () => {
  const { handleSubmit, register } = useForm();
  const [tasks, setTasks] = useState(data);

  const formParagraph = [
    {
      id: 1,
      paragraph: (
        <div class="field">
          <label class="label">Paragraph</label>
          <textarea
            type="text"
            ref={register}
            name="about"
            class="Normal textarea"
            placeholder="Add a Paragraph here, split them as you like"
            rows="10"
          ></textarea>
        </div>
      ),
    },
  ];

  const [field, setfields] = useState(formParagraph);

  const plusOneParagraph = () => {
    const additionalField = {
      paragraph: <div class="field">
      <label class="label">Paragraph</label>
      <textarea
        type="text"
        ref={register}
        name="about"
        class="Normal textarea"
        placeholder="Add a Paragraph here, split them as you like"
        rows="10"
      ></textarea>
    </div>
    }
    setfields(...formParagraph, additionalField)
  }
  const onClickAddFunction = (userInputData) => {
    console.log("=============clicked=============");
    db.collection("article")
      .add(userInputData)
      .then((ref) => {
        console.log("added user created data: ", ref.id);
        // added user input
      });
  };

  return (
    <>
      <BasicHeader />

      <section
        class="hero is-primary is-medium"
        className="medium-hero"
        style={{ marginBottom: "10px" }}
      ></section>
      <div style={{ margin: "15px 5%" }}>
        <form onSubmit={handleSubmit(onClickAddFunction)}>
          <div class="field">
            <label class="label">Title</label>
            <div class="control">
              <input
                class="input"
                type="text"
                placeholder="example"
                ref={register}
                name="title"
              />
            </div>
          </div>

          <div class="columns">
            <div class="column">
              <div class="columns is-mobile">
                <div class="column is-one-third">
                  <div class="field">
                    <label class="label">Description</label>
                    <div class="control">
                      <input
                        class="input"
                        type="text"
                        placeholder="example"
                        ref={register}
                        name="description"
                      />
                    </div>
                  </div>
                </div>
                <div class="column is-one-third">
                  <div class="field">
                    <label class="label">About</label>
                    <div class="control">
                      <input
                        class="input"
                        type="text"
                        placeholder="example"
                        ref={register}
                        name="about"
                      />
                    </div>
                  </div>
                </div>
                <div class="column is-one-third">
                  <div class="field">
                    <label class="label">Length</label>
                    <div class="control">
                      <input
                        class="input"
                        type="text"
                        placeholder="example"
                        ref={register}
                        name="length"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div>
        {tasks.map((task) => (
          <p>{task.dataItem}</p>
        ))}
      </div>

      <div>
        {field.map((para) => (
          <div>{para.paragraph}</div>
        ))}
      </div>

          <div class="field is-grouped">
            <div class="control">
              <button class="button is-link">Submit</button>
            </div>
          </div>
        </form>

        <div class="field is-grouped">
          <div class="control">
            <button class="button is-secondary" onClick={plusOneParagraph}>
              Add
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default ArticleCreate;
