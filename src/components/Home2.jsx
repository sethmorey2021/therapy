import React from "react";
// import BasicHeader from "./BasicHeader";
import FancyNav from './dataComponents/FancyNav'
import Footer from "./dataComponents/Footer";
import LayoutCopy from "./dataComponents/LayoutCopy";

const Home = () => {
  return (
    <div className="Home">
      <section className="hero is-info is-large" id="gif-background">
        <FancyNav/>
        <div className="hero-body">
          <div className="container">
            <h1 className="title">Above all else, guard your heart, for it is the wellspring of life."</h1>
            <h2 className="subtitle">Proverbs 4:23</h2>
          </div>
        </div>
      </section>
      <div className="gutter"><LayoutCopy/></div>
      <Footer/>
      
    </div>
  );
};

export default Home;
