import React from "react";
import covervid from "../images/SkyVid.mp4";

export default function VideoBackground() {
  return (
    <div>
      <div>
        <video loop autoPlay>
          <source src={covervid} type="video/mp4" />
          Your browser does not support the video tag. I suggest you upgrade your browser.
        </video>
      </div>
    </div>
  );
}
