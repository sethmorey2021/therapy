import React from "react";
import { useState } from "react";
import { useHistory, Link } from 'react-router-dom'
import { useForm } from 'react-hook-form'
import { useAuth } from '../context/AuthContext'

const Login = () => {
  const history = useHistory()
  const { handleSubmit, register, getValues } = useForm();
  const { login } = useAuth()
  const [loading, setLoading] = useState()
  const [error, setError] = useState(false)

  // const logSubmit = (data) => console.log(data)

  const handleLogIn =  async () => {
    const emailValue = getValues('email')
    const passwordValue = getValues('password')
    // console.log(emailValue, passwordValue)

    try {
      setError("");
      setLoading(true);
      await login(emailValue, passwordValue)
      history.push("/account")
    } catch {
      setError("Failed to Login")
    }

    setLoading(false)
  }

  return (
    <>
      <div className="gutter">
        <form onSubmit={handleSubmit(handleLogIn)}>
          <h1 className="title">Log In</h1>
          <div class="field">
            <label class="label">Email</label>
            <div class="control">
              <input
                name="email"
                class="input"
                type="text"
                placeholder="@email.com"
                ref={register}
              />
            </div>
          </div>

          <div class="field">
            <label class="label">Password</label>
            <div class="control">
              <input
                name="password"
                class="input"
                type="password"
                placeholder="password"
                ref={register}
              />
            </div>
          </div>

          <button class="button is-primary" type="submit">
            Log In
          </button>

          <Link to="/signup">
            <button class="button is-link" style={{ marginLeft: "10px" }}>
              Sign Up
            </button>
          </Link>

        </form>
      </div>
    </>
  );
};

export default Login
