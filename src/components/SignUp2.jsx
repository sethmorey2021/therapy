import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { useHistory, Link } from "react-router-dom";
import { useAuth } from "../context/AuthContext";

const Signup = () => {
  const { handleSubmit, register, getValues } = useForm();
  const history = useHistory();
  const { signup } = useAuth();
  const [loading, setLoading] = useState();
  const [error, setError] = useState(false);

  // const onSubmit = (data) => console.log(data)

  const handleSignUp = async () => {
    const emailValue = getValues("email");
    const passwordValue = getValues("password")
    try {
      setError("");
      setLoading(true);
      await signup(emailValue, passwordValue)
      history.push("/account");
    } catch {
      setError("Failed to SignUp");
    }

    setLoading(false);
  };

  return (
    <div className="gutter">
      <form onSubmit={handleSubmit(handleSignUp)}>
        <div>
          <div className="field">
            <label className="label">Email</label>
            <div className="control">
              <input
                name="email"
                ref={register}
                className="input is-medium"
                type="text"
                placeholder="Medium input"
              />
            </div>
          </div>

          <div className="field">
            <label className="label">Password</label>
            <div className="control">
              <input
                name="password"
                ref={register}
                className="input is-medium"
                type="text"
                placeholder="Medium input"
              />
            </div>
          </div>
        </div>
        {/* <button className="primary button" type="submit" onClick={(e) => {
        e.preventDefault()
        const values = getValues();
        const emailValue = getValues("email")
        console.log(emailValue)
      }}>Sign Up</button> */}
        <button className="primary button" type="submit">
          Sign Up
        </button>

        <Link to="/login">
          <button class="button is-link" style={{ marginLeft: "10px" }}>
            Login
          </button>
        </Link>
      </form>
    </div>
  );
};

export default Signup;
