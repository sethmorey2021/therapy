import React from "react";
import { Link } from "react-router-dom";

const BasicHeader = () => {
  return (
    <div className="is-block">
      <nav class="navbar is-transparent">
        <div class="navbar-brand">
          <Link class="navbar-item" to="/home">
            <p className="header-logo">Danielle Cutrer, LPC</p>
          </Link>
        </div>

        <div id="navbarExampleTransparentExample" class="navbar-menu">
          <div class="navbar-start">
            <div class="navbar-item has-dropdown is-hoverable">
              <p
                class="navbar-link"
                href="https://bulma.io/documentation/overview/start/"
              ></p>
              <div class="navbar-dropdown is-boxed">
                <Link to="/about">
                  <p
                    class="navbar-item"
                    href="https://bulma.io/documentation/overview/start/"
                  >
                    About
                  </p>
                </Link>
                <Link to="/contact">
                  <p
                    class="navbar-item"
                    href="https://bulma.io/documentation/overview/start/"
                  >
                    Contact
                  </p>
                </Link>
                <Link to="/faq">
                  <p
                    class="navbar-item"
                    href="https://bulma.io/documentation/overview/start/"
                  >
                    FAQ
                  </p>
                </Link>
                <Link to="/blog">
                  <p
                    class="navbar-item"
                    href="https://bulma.io/documentation/overview/start/"
                  >
                    Blog
                  </p>
                </Link>
                <Link to="/forms">
                  <p
                    class="navbar-item"
                    href="https://bulma.io/documentation/overview/start/"
                  >
                    Forms
                  </p>
                </Link>
              </div>
            </div>
          </div>

          <div class="navbar-end">
            <div class="navbar-item">
              <div class="field is-grouped">
                <Link to="/login">
                  <button class="button is-primary is-outlined">Account</button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default BasicHeader;
