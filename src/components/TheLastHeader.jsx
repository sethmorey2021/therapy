import React from "react";

const TheLastHeader = () => {
  return (
    <div>
      <header>
        <ul className="thelastheader">
          <div class="flex-child">Flex Column 1</div>
          <div class="flex-child flex-again">
            <li className="thelastheader-item">link</li>
            <li className="thelastheader-item">link</li>
            <li className="thelastheader-item">link</li>
            <li className="thelastheader-item">link</li>
            <li className="thelastheader-item">link</li>
          </div>
        </ul>
      </header>
    </div>
  );
};

export default TheLastHeader;
