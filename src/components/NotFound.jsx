import React from "react";

const NotFound = () => {
  return (
    <div style={{ textAlign: "center" }} className="gutter">
      <h1>Not Found</h1>
      <img
        src="https://media.giphy.com/media/3o7aTskHEUdgCQAXde/giphy.gif"
        alt="not found gif"
        // style={{height: '500px', width: '800'}}
      />
    </div>
  );
};

export default NotFound;
