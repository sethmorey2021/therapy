import React from "react";
import jsonServer from "../apis/jsonServer";
import { useForm } from "react-hook-form";
import BasicHeader from "./BasicHeader";
import { db } from "../Firebase";
import Footer from "../components/dataComponents/Footer";

const Contact = () => {
  const { handleSubmit, register } = useForm();

  const sendToApi = (formData) => {
    jsonServer.post("/data2", formData);
  };

  // const onSubmit = (data) => console.log(data);

  const onClickAddFunction = (userInputData) => {
    console.log("=============clicked=============");
    db.collection("contact")
      .add(userInputData)
      .then((ref) => {
        console.log("added user created data: ", ref.id);
        // added user input
      });
  };

  return (
    <>
      <BasicHeader />
      <section
        class="hero is-primary is-medium"
        className="medium-hero"
        style={{ marginBottom: "10px" }}
      ></section>

      <div className="gutter">
        
      </div>
      <div className="contact-form-border">
        <form onSubmit={handleSubmit(onClickAddFunction)}>

          <div class="columns">
            <div class="column">
              <div class="columns is-mobile">
                <div class="column is-half">
                  <div class="field">
                    <label class="label">First</label>
                    <div class="control">
                      <input
                        class="input"
                        type="text"
                        placeholder="First Name"
                        ref={register}
                        name="first"
                      />
                    </div>
                  </div>
                </div>
                <div class="column">
                  <div class="field">
                    <label class="label">Last</label>
                    <div class="control">
                      <input
                        class="input"
                        type="text"
                        placeholder="Last Name"
                        ref={register}
                        name="last"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="columns">
            <div class="column">
              <div class="columns is-mobile">
                <div class="column is-half">
                  <div class="field">
                    <label class="label">email</label>
                    <div class="control">
                      <input
                        class="input"
                        type="text"
                        placeholder="Email"
                        ref={register}
                        name="email"
                      />
                    </div>
                  </div>
                </div>
                <div class="column">
                  <div class="field">
                    <label class="label">phone</label>
                    <div class="control">
                      <input
                        class="input"
                        type="text"
                        placeholder="Phone"
                        ref={register}
                        name="phone"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="columns">
            <div class="column">
              <div class="field">
                <label class="label">Message</label>
                <textarea
                  type="text"
                  ref={register}
                  name="message"
                  class="Normal textarea"
                  placeholder="Message Here"
                  rows="10"
                ></textarea>
              </div>
            </div>
          </div>

          <div class="field is-grouped">
            <div class="control">
              <button class="button is-link">Submit</button>
            </div>
          </div>
        </form>
      </div>
      <Footer />
    </>
  );
};

export default Contact;
