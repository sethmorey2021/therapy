import React from "react";
import BasicHeader from "./BasicHeader";

const Forms = () => {
  return (
    <>
      <BasicHeader />
      <div className="gutter">
        <div class="columns" style={{ textAlign: "center" }}>
          <div class="column">
            <button class="button is-link">Form for Forms Sake</button>
          </div>
          <div class="column">
            <button class="button is-link">Form for Forms Sake</button>
          </div>
        </div>
        <div class="columns" style={{ textAlign: "center" }}>
          <div class="column">
            <button class="button is-link">Form for Forms Sake</button>
          </div>
          <div class="column">
            <button class="button is-link">Form for Forms Sake</button>
          </div>
        </div>
        <div class="columns" style={{ textAlign: "center" }}>
          <div class="column">
            <button class="button is-link">Form for Forms Sake</button>
          </div>
          <div class="column">
            <button class="button is-link">Form for Forms Sake</button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Forms;
