import React, { useContext, useState } from "react";
import PortalModal from "../components/PortalModal";
// import Card from "../components/Card";
import { db } from "../Firebase";
import { useForm } from "react-hook-form";
import jsonServer from "../apis/jsonServer";
import Card from "../components/basicComps/Card";
import Article from "./basicComps/Article";
import { ColorContext } from "../ColorContext";

// const cityRef = db.collection("client");
// const doc = cityRef.get();
// if (!doc.exists) {
//   console.log("No such document!");
// } else {
//   console.log("Document data:", doc.data());
// }

const booksRef = db.collection("client");

booksRef.get().then((snapshot) => {
  const data = snapshot.docs.map((doc) => ({
    id: doc.id,
    ...doc.data(),
  }));
  console.log("All data in 'books' collection", data);
  // [ { id: 'glMeZvPpTN1Ah31sKcnj', title: 'The Great Gatsby' } ]
});

const clientRefDoc = db.collection("client").doc("cf0Mdt2QKzEaRPwpdGAd");

clientRefDoc.get().then((doc) => {
  if (!doc.exists) return;
  console.log("Document data:", doc.data());
  // Document data: { title: 'The Great Gatsby' }
});

const fetchFromFirebase = () => {
  console.log("call me");
};

// db
//   .collection("books")
//   .doc("another book")
//   .set({
//     title: "War and Peace",
//   })
//   .then(() => {
//     console.log("Document created");
//   });

db.collection("client");

const addEntryToBookCollection = (item) => {
  db.collection("books")
    .doc("another book")
    .set({
      title: item,
    })
    .then(() => {
      console.log(`title entered: ${item}`);
    });
};

const addEntryToBookCollection2 = (item) => {
  db.collection("books")
    .doc("whatever")
    .set({
      item,
    })
    .then(() => {
      console.log(`title entered: ${item}`);
    });
};

const onClickFetchBoosk = () => {
  console.log("**************** clicked *******************");
  booksRef.get().then((snapshot) => {
    const data = snapshot.docs.map((doc) => ({
      id: doc.id,
      ...doc.data(),
    }));
    console.log("All data in 'books' collection", data);
    // [ { id: 'glMeZvPpTN1Ah31sKcnj', title: 'The Great Gatsby' } ]
  });
};

const onClickAddFunction = (userInputData) => {
  console.log("=============clicked=============");
  db.collection("books")
    .add(userInputData)
    .then((ref) => {
      console.log("added user created data: ", ref.id);
      // added user input
    });
};

// db
//   .collection("books")
//   .add({
//     title: "where does this go",
//   })
//   .then((ref) => {
//     console.log("Added doc with ID: ", ref.id);
//     // Added doc with ID:  ZzhIgLqELaoE3eSsOazu
//   });



const ExamplePractice = () => {
  const [isOpen, setisOpen] = useState(false);

  const { handleSubmit, register } = useForm();

  const sendToApi = ({ item }) => {
    jsonServer.post("/data2", item);
  };
  
  const colors = useContext(ColorContext);
  console.log("here we are", colors)

  return (
    <div className="gutter">
      <button onClick={() => setisOpen(true)} className="primary button">
        Modal
      </button>

      <PortalModal open={isOpen} onClose={() => setisOpen(false)}>
        Fancy Modal
      </PortalModal>

      <div>hi there</div>

      <Card title={"Client"} />

      <div style={{ height: "100vh" }}>
        <h1>Document Render Example</h1>

        <form onSubmit={handleSubmit(addEntryToBookCollection2)}>
          <div class="field">
            <label class="label">Client Name</label>
            <div class="control">
              <input
                class="input"
                type="text"
                placeholder="example"
                ref={register}
                name="item"
              />
            </div>
          </div>

          {/* <div class="field">
            <label class="label">Email</label>
            <div class="control">
              <input
                class="input"
                type="text"
                placeholder="example"
                ref={register}
                name="email"
              />
            </div>
          </div> */}

          <div class="field">
            <div class="control">
              <label class="checkbox">
                <input type="checkbox" />
              </label>
            </div>
          </div>

          <div class="field is-grouped">
            <div class="control">
              <button class="button is-link">Submit</button>
            </div>
          </div>
        </form>
        <div>
          <button
            className="button is-link"
            onClick={onClickFetchBoosk}
            style={{ marginTop: "1em" }}
          >
            Fetch
          </button>
        </div>

        <form onSubmit={handleSubmit(onClickAddFunction)}>
          <div class="field">
            <label class="label">Client Name</label>
            <div class="control">
              <input
                class="input"
                type="text"
                placeholder="example"
                ref={register}
                name="item"
              />
            </div>
          </div>

          <div class="field">
            <div class="control">
              <label class="checkbox">
                <input type="checkbox" />
              </label>
            </div>
          </div>

          <div class="field is-grouped">
            <div class="control">
              <button class="button is-link">Submit</button>
            </div>
          </div>
        </form>
      </div>

      <div class="columns is-mobile is-multiline is-centered">
        <div class="column is-narrow">
          <p class="bd-notification is-primary">
            <code class="html">is-narrow</code>
            First Column
          </p>
        </div>
        <div class="column is-narrow">
          <p class="bd-notification is-primary">
            <code class="html">is-narrow</code>
            Our Second Column
          </p>
        </div>
        <div class="column is-narrow">
          <p class="bd-notification is-primary">
            <code class="html">is-narrow</code>
            Third Column
          </p>
        </div>
        <div class="column is-narrow">
          <p class="bd-notification is-primary">
            <code class="html">is-narrow</code>
            The Fourth Column
          </p>
        </div>
        <div class="column is-narrow">
          <p class="bd-notification is-primary">
            <code class="html">is-narrow</code>
            Fifth Column
          </p>
        </div>
      </div>
    </div>
  );
};

export default ExamplePractice;
