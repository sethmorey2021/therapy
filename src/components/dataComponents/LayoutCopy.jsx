import React from "react";

function LayoutCopy() {
  return (
    <>
      <div style={{ marginTop: "50px" }}>
        <h1 style={{ fontSize: "2rem", fontWeight: "bold" }}>
          Section Heading
        </h1>
      </div>
      <div className="photo-group">
        <div class="columns">
          <div class="column is-narrow is-half">
            <figure class="image is-900 x 1500">
              <img
                src="https://cdn.pixabay.com/photo/2020/07/23/09/59/runswick-bay-5430911_960_720.jpg"
                alt="photos"
              />
            </figure>
            <div style={{ marginTop: "25px" }}>
              <p style={{ fontSize: "2rem", fontWeight: "bold" }}>
                You are not Alone
              </p>
              <p>
                Every person has experienced pain at one time or another. The
                kind that brings us to the end of us. The kind that takes our
                breath from our bodies, leaves us sick in our soul, and brings
                us to our knees. It is the kind that leaves us dazed an unsure
                of what to do next. It is in this pain that we reach out for
                another. When we finally say " I cannot go it alone." It is in
                this place the true connection is created.
              </p>
            </div>
          </div>
          <div class="column is-narrow is-half">
            <figure class="image is-900 x 1500">
              <img
                src="https://cdn.pixabay.com/photo/2020/07/23/09/59/runswick-bay-5430911_960_720.jpg"
                alt="photos"
              />
            </figure>
            <div style={{ marginTop: "25px" }}>
              <p style={{ fontSize: "2rem", fontWeight: "bold" }}>
                The Power of Vulnerability
              </p>
              <p>Link to Ted Talk</p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default LayoutCopy;
