import React from 'react'

function Footer() {
  return (
    <div className="footer">
      All Content Owned and Operated by Private Practice
    </div>
  )
}

export default Footer
