import React from "react";
import { Link } from "react-router-dom";

function FancyNav() {
  return (
    <div>
      <nav
        class="navbar"
        role="navigation"
        aria-label="main navigation"
        id="modified-nav"
      >
        <div class="navbar-brand">
          <a class="navbar-item" href="https://bulma.io">
            <p className="header-logo">Danielle Cutrer, Licensed Professional Counselor</p>
          </a>

          <div
            role="button"
            class="navbar-bpurger"
            aria-label="menu"
            aria-expanded="false"
            data-target="navbarBasicExample"
          >
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </div>
        </div>

          <div class="navbar-end">
            <div class="navbar-item"><div
              class="navbar-item hover-underline-animation"
              style={{ color: "white" }}
            >
              <Link to="/about">About Me</Link>
            </div>
            <div
              class="navbar-item hover-underline-animation"
              style={{ color: "white" }}
            >
              <Link to="/contact">Contact</Link>
            </div>
            <div
              class="navbar-item hover-underline-animation"
              style={{ color: "white" }}
            >
              <Link to="/forms">Forms</Link>
            </div>
            <div
              class="navbar-item hover-underline-animation"
              style={{ color: "white" }}
            >
              <Link to="/blog">Blog</Link>
            </div>

            <div
              class="navbar-item hover-underline-animation"
              style={{ color: "white" }}
              id="little-space"
            >
              <Link to="/faq">FAQ</Link>
            </div>
              <div class="buttons">
                <Link to="/login">
                  <button class="button is-primary is-outlined">Account</button>
                </Link>
              </div>
            </div>
          </div>
      </nav>
    </div>
  );
}

export default FancyNav;
