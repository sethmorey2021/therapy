import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { useAuth } from "../context/AuthContext";

export default function Account() {
  const [error, setError] = useState("");
  const { currentUser, logout } = useAuth();
  const history = useHistory();

  async function handleLogout() {
    setError("");

    try {
      await logout();
      history.push("/login");
    } catch {
      setError("Failed to log out");
    }
  }

  return (
    <>
      <h2>Profile</h2>
      {error && (
        <div class="notification is-danger">
          <button class="delete"></button>
          {error}
        </div>
      )}
      <h1 className="title 1"></h1> {currentUser.email}
      <div></div>
      <Link to="/update-profile">
        <button className="secondary button">Update Profile</button>
      </Link>
      <div></div>
      <button className="secondary button" onClick={handleLogout}>
        Log Out
      </button>
    </>
  );
}
