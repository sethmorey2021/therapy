import React from "react";
import { Link } from "react-router-dom";

const Welcome = () => {
  return (
    <>
      <div className="hero-image">
        <section class="hero is-fullheight">
          <div class="hero-body">
            <div class="container has-text-centered">
              <p class="title" style={{ color: "white" }}>
                Welcome
              </p>
              <Link to="/home">
                <button class="button is-primary is-light">
                  Press Here to Enter
                </button>
              </Link>
            </div>
          </div>
        </section>
      </div>
    </>
  );
};

export default Welcome;
