import React, { useContext, useEffect, useState } from "react";
import Card from "./basicComps/Card";
import BasicHeader from "./BasicHeader";
import { db } from "../Firebase";
import { defaultBlogContext } from "../context/FirebaseBlogContext";
import Footer from "./dataComponents/Footer";
import FirebaseBlogContext from '../context/FirebaseBlogContext';

const title = "My Title";
const author = "Author Name";
const ipsumLorem =
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pulvinar nunc ac posuere iaculis. Suspendisse ac justo malesuada quam vestibulum consequat.";

const Blog = (FirebaseBlogContext) => {
  const fetchAllBlogs = "fetch me";

  const CardContext = React.createContext()

  const blogsRef = db.collection("blogs");
  const [firebaseBlogs, setFirebaseBlogs] = useState([]);
  // const context = useContext(defaultBlogContext)

  useEffect(() => {
    blogsRef.onSnapshot((snapshot) => {
      const data = snapshot.docs.map((doc) => ({
        id: doc.id,
        ...doc.data(),
      }));
      setFirebaseBlogs(data);
    });
  });

  return (
    <>
      <BasicHeader />
      <section
        class="hero is-primary is-medium"
        className="medium-hero"
        style={{ marginBottom: "25px" }}
      ></section>

      <div className="gutter">
        <div className="columns is-multiline is-mobile">
          {firebaseBlogs.map((blog) => (
            <div class="column is-narrow is-one-third-desktop is-half-tablet is-full-mobile">
              <Card
                title={blog.title}
                author={blog.author}
                content={blog.content}
                blogId={blog.id}
              />
            </div>
          ))}
        </div>
      </div>

      <Footer />
    </>
  );
};

export default Blog;
