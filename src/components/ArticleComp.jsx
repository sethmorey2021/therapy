import React from 'react'
import Article from '../components/basicComps/Article'


const title = "My Title";
const author = "Written by Me";
const content =
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Diam quis enim lobortis scelerisque fermentum dui faucibus in ornare. Lacus vestibulum sed arcu non odio euismod. Tristique risus nec feugiat in. Rhoncus est pellentesque elit ullamcorper. Donec ac odio tempor orci dapibus ultrices in iaculis nunc. Senectus et netus et malesuada fames ac turpis. Euismod nisi porta lorem mollis. Enim sit amet venenatis urna cursus eget. Est sit amet facilisis magna etiam tempor orci eu. Vestibulum mattis ullamcorper velit sed ullamcorper morbi.";

  const ArticleComp = () => {
  return (
    <div>
      <Article title={title} author={author} content={content}/>
    </div>
  )
}

export default ArticleComp
