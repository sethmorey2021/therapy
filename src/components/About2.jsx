import React from "react";
import BasicHeader from "./BasicHeader";

const About2 = () => {
  return (
    <>
      <BasicHeader />
      <section
        class="hero is-primary is-medium"
        className="medium-hero"
      ></section>
      <div className="gutter" style={{ marginBottom: "5%" }}>
        <div className="about-container">
          <h3 class="title is-3">About Me</h3>
          <div class="columns is-vcentered">
            <div class="column is-4">
              <img className="" src="https://cdn.pixabay.com/photo/2021/01/30/15/15/dog-5964181_960_720.jpg"/>
            </div>
            <div class="column">
              <p>
                Danielle Cutrer, MS, LPC is a Licensed Professional Counselor
                who is a native to Phoenix, Arizona. She graduated from the
                University of Phoenix with her Bachelor of Science in Psychology
                and Master of Science in Clinical Mental Health Counseling. She
                previously worked as a youth intern for 2 years and a youth
                pastor for 3 years at The Summit Church. Danielle has worked in
                the mission field with a non-profit missions organization,
                traveling to Jamaica 3 separate times and also Scotland. It was
                through this organization that she eventually became a project
                manager and organized teams from all around the country to
                complete their own mission trips. She enjoys working with teens,
                young adults, career-aged individuals and couples. She is an
                EMDR trained therapist and strongly believes in the benefit of
                trauma resolution through this technique. She completed her
                internship with Christian Family Care Agency in Phoenix, AZ
                where she studied Attachment Theory and Emotion Focused Couples
                Therapy (EFCT) for couples.​ She uses EFCT interventions with
                couples that focus on creating safety and security through
                healthy partner attachment. Danielle also is an Arizona Board of
                Behavioral Health Examiners approved clinical supervisor. She
                can provide board approved supervision to both intern/practicum
                students and Licensed Associate Counselors. She currently has
                practicum students working with her and is able to offer
                counseling at a low cost fee for clients who wish to see them!
                By incorporating a faith based approach to counseling, she seeks
                to help each client find hope. She has a deep love for healthy
                living, nutrition, her husband Joseph and daughter Gabrielle,
                and a Christ-centered life.
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default About2;
