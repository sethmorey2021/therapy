import React, { useState } from "react";
import PortalModal from "../components/PortalModal";
import Card from '../components/Card';
import TheLastHeader from "./TheLastHeader";

const Practice = () => {
  const [isOpen, setisOpen] = useState(false);
  return (
    <div className="gutter">
      <button onClick={() => setisOpen(true)} className="primary button">
        Modal
      </button>

      <PortalModal open={isOpen} onClose={() => setisOpen(false)}>
        Fancy Modal
      </PortalModal>

      <TheLastHeader />

      <Card title={"Client"} />
      <Card />
      <Card />
      <Card />
      <Card />
      <Card />

      <div class="column">
        <p class="bd-notification is-danger">Section Title</p>
        <div class="columns is-mobile">
          <div class="column is-half">
            <Card title="Client" />
          </div>
          <div class="column is-half">
            <Card title="Client" />
          </div>
        </div>
      </div>

      <div class="outer">
        <div class="bot">BOT</div>
        <div class="top">TOP</div>
      </div>

      <div className="container">
        <div className="item-1">1</div>
        <div className="item-2">2</div>
        <div className="item-3">3</div>
      </div>

      <div class="columns is-gapless is-multiline is-mobile">
        <div class="column is-one-quarter">
          <figure class="image is-900 x 1500">
            <img
              src="https://cdn.pixabay.com/photo/2017/10/25/16/54/african-lion-2888519_960_720.jpg"
              alt="photos"
            />
          </figure>
        </div>
        <div class="column is-one-quarter">
          <figure class="image is-900 x 1500">
            <img
              src="https://cdn.pixabay.com/photo/2017/10/25/16/54/african-lion-2888519_960_720.jpg"
              alt="photos"
            />
          </figure>
        </div>
        <div class="column is-one-quarter">
          <figure class="image is-900 x 1500">
            <img
              src="https://cdn.pixabay.com/photo/2017/10/25/16/54/african-lion-2888519_960_720.jpg"
              alt="photos"
            />
          </figure>
        </div>
        <div class="column is-one-quarter">
          <figure class="image is-900 x 1500">
            <img
              src="https://cdn.pixabay.com/photo/2017/10/25/16/54/african-lion-2888519_960_720.jpg"
              alt="photos"
            />
          </figure>
        </div>
        <div class="column is-one-quarter">
          <figure class="image is-900 x 1500">
            <img
              src="https://cdn.pixabay.com/photo/2017/10/25/16/54/african-lion-2888519_960_720.jpg"
              alt="photos"
            />
          </figure>
        </div>
        <div class="column is-one-quarter">
          <figure class="image is-900 x 1500">
            <img
              src="https://cdn.pixabay.com/photo/2017/10/25/16/54/african-lion-2888519_960_720.jpg"
              alt="photos"
            />
          </figure>
        </div>
        <div class="column is-one-quarter">
          <figure class="image is-900 x 1500">
            <img
              src="https://cdn.pixabay.com/photo/2017/10/25/16/54/african-lion-2888519_960_720.jpg"
              alt="photos"
            />
          </figure>
        </div>
        <div class="column is-one-quarter">
          <figure class="image is-900 x 1500">
            <img
              src="https://cdn.pixabay.com/photo/2017/10/25/16/54/african-lion-2888519_960_720.jpg"
              alt="photos"
            />
          </figure>
        </div>
      </div>
    </div>
  );
};

export default Practice;
