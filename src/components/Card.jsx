import React from "react";

export default function Card({ title }) {
  return (
    <div>
      <div className="card" style={{marginBottom: '10px'}}>
        <header className="card-header">
          <p className="card-header-title">{title}</p>
          <a href="#" className="card-header-icon" aria-label="more options">
          </a>
        </header>
        <div className="card-content">
          <div className="content">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec iaculis mauris.
            <a href="#">@bulmaio</a>. <a href="#">#css</a> <a href="#">#responsive</a>
            <br />
            <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
          </div>
        </div>
        <footer className="card-footer">
          <a href="#" className="card-footer-item">
            Details
          </a>
          <a href="#" className="card-footer-item">
            Option
          </a>
          <a href="#" className="card-footer-item">
            Option
          </a>
        </footer>
      </div>
    </div>
  );
}
