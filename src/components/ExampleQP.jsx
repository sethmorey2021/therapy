import React from "react";
import { Link } from "react-router-dom";
import { useParams } from "react-router";

function User() {
  let { id } = useParams();
  return <h2>User {id}</h2>;
}

const dynamicId = 1234;

const ExampleQP = () => {
  return (
    <>
      <div className="gutter">
        <div>
          <Link to="/article/1">User 1</Link>
        </div>
        <div>
          <Link to="/article/2">User 2</Link>
        </div>
        <div>
          <Link to={`/article/${dynamicId}`}>dynamicId</Link>
        </div>
      </div>
    </>
  );
};

export default ExampleQP;
